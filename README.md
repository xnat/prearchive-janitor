# XNAT Prearc Janitor

Tidies up broken imports int the prearchive for some error cases(error handling
can be easily extended).
Because there is now Signal from Syngoplaza that a transmission of a new scan is
over Xnat just waits 5 minutes and than starts archiving.
Sometimes the C-Store SEND needs longer and sesions get split up.

For following error cases and all combinations of them the files get merged:
* 1 - Session already exists, retry with overwrite enabled
* 10 - Session processing may already be in progress: AutoRun. Concurrent modification is discouraged.
* 14 - Session already exists with matching files.
* 16 - Session already contains a scan (4) with the same UID and number. 

To start the prearchive janitor just call the Restendpoint with GET:
`xnat.example.com/xnat/services/extensions/tidyuppreachrive`


## Installation:

1. Build .zip (manually or via script)

    Make sure the structure of the zip archive is like this:
    ```
    src/
    ├── java
       └── org
           └── nrg
               └── xnat
                   ├── restlet
                         └── extensions
                                 └── PrearcJanitor.java
    ```

2. Move .zip to your xnat module directory

        mv prearchive-janitor.zip xnat-1.6.5/xnat/modules

3. Update and redeploy XNAT
