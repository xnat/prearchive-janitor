package org.nrg.xnat.restlet.extensions;

import org.nrg.action.ClientException;
import org.nrg.status.StatusListenerI;
import org.nrg.xft.XFTTable;
import org.nrg.xft.exception.InvalidPermissionException;
import org.nrg.xnat.archive.FinishImageUpload;
import org.nrg.xnat.archive.PrearcSessionValidator;
import org.nrg.xnat.helpers.prearchive.PrearcDatabase;
import org.nrg.xnat.helpers.prearchive.PrearcUtils;
import org.nrg.xnat.helpers.prearchive.SessionException;
import org.nrg.xnat.helpers.uri.URIManager;
import org.nrg.xnat.helpers.uri.UriParserUtils;
import org.nrg.xnat.restlet.XnatRestlet;
import org.nrg.xnat.restlet.actions.PrearcImporterA.PrearcSession;
import org.nrg.xnat.restlet.resources.SecureResource;
import org.restlet.Context;
import org.restlet.data.MediaType;
import org.restlet.data.Request;
import org.restlet.data.Response;
import org.restlet.data.Status;
import org.restlet.resource.Representation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.StringRepresentation;
import org.restlet.resource.Variant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.sql.SQLException;
import java.util.*;

/**
 * Searches broken import in the prearchive  merges with the archive for specific errors cases.
 * Some programs like Syngoplaza sending Dicom files via DICOM C-STORE sometimes need longer than expected
 * for transmitting a complete session. This leads to half archive sessions with this class can merge for all prearc entries.
 * @author  paul
 */
@XnatRestlet({"/services/extensions/tidyuppreachrive", "/services/extensions/tidyuppreachrive/{SESSION_ID}"})
public class PrearcJanitor extends SecureResource {

    private static final Logger logger = LoggerFactory.getLogger(PrearcJanitor.class);

    String currentPreArcEntry;
    int fixedSessions;

    final Map<String, Object> additionalValues = new Hashtable<String, Object>();


    public PrearcJanitor(Context context, Request request, Response response) {
        super(context, request, response);
        this.getVariants().add(new Variant(MediaType.ALL));
        this.fixedSessions = 0;
    }

    public void handleParam(final String key, final Object value) throws ClientException {
        additionalValues.put(key, value);
    }

    /**
     * triggers a scan of the prearchive trying to fix failed imports;called via GET
     *
     * @param variant
     * @return Stringrepresentation with amount of fixed sessions
     * @throws ResourceException
     */
    @Override
    public Representation represent(Variant variant) throws ResourceException {
        try {
            if (!user.checkRole(PrearcUtils.ROLE_SITE_ADMIN)) {
                this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN, "Non admin user's cant access this service!");
                logger.error("Non Admin user tried to query PrearcJanitor!");
                return null;
            }
        } catch (Exception e) {
            logger.error("Error check Userrole: "+e.toString());
        }
        logger.info("JANITOR test");
        XFTTable preArcSessions = this.getPrearchiveEntries();
        HashMap<String,Integer> errorOccurrence = new HashMap<>(); // dictionary which collects how often an error combinations occurs

        for(int i=0;i<preArcSessions.size();i++){
            PrearcSession session = this.getPrearcSession(preArcSessions.nextRow());
            List<PrearcSessionValidator.Notice> errors = this.getPrearcImportErrors(session);
            if(errors.size()>0){
                String errorCombo = this.getErrorStringfromList(errors);

                if(errorOccurrence.containsKey(errorCombo)){ // adds error combo to statistic dictionary
                    errorOccurrence.put(errorCombo,errorOccurrence.get(errorCombo)+1);
                }else{
                    errorOccurrence.put(errorCombo,1);
                }
                switch (errorCombo){
                    case "1/":          this.archiveSessiosWithErrors(session);break; //Session already exists, retry with overwrite enabled
                    case "10/":         this.archiveSessiosWithErrors(session);break; //Session processing may already be in progress: AutoRun. Concurrent modification is discouraged.
                    case "14/":         this.archiveSessiosWithErrors(session);break; //Session already exists with matching files.
                    case "16/":         this.archiveSessiosWithErrors(session);break; //Session already contains a scan (4) with the same UID and number.
                    case "1/16/":       this.archiveSessiosWithErrors(session);break; 
                    case "1/10/16/":    this.archiveSessiosWithErrors(session);break; 
                    case "1/14/16/":    this.archiveSessiosWithErrors(session);break;
                    case "1/10/14/16/": this.archiveSessiosWithErrors(session);break; 
                    case "10/14/":      this.archiveSessiosWithErrors(session);break;
                    case "10/14/16/":   this.archiveSessiosWithErrors(session);break;
                    default: logger.error("Unknown error combo: " + errorCombo + "Session: " + currentPreArcEntry); break;
                }
            }
        }

        logger.info("Prearchive Error Statistic before fixing Errors:");
        for (Map.Entry<String, Integer> entry : errorOccurrence.entrySet()) {
            logger.info("Error combo: "+entry.getKey()+" Occurrence: "+entry.getValue());
        }
        return new StringRepresentation("Janitor fixed sessions: "+this.fixedSessions); // TODO redirect to start page
    }

    /**
     * triggers a archiving of a given sessions with allowSessionMerge enabled
     * @param session which should me merged
     */
    private void archiveSessiosWithErrors(PrearcSession session){
        boolean overrideExceptions = true;
        boolean allowSessionMerge = true;
        boolean overwrite_files = false;
        String newarchiveLocation = "";
        logger.info("Fixing brocken session: "+currentPreArcEntry);
        Set<StatusListenerI> listeners=(Set<StatusListenerI>)Collections.EMPTY_SET;
        try{
            if(PrearcDatabase.setStatus(session.getFolderName(), session.getTimestamp(), session.getProject(), PrearcUtils.PrearcStatus.ARCHIVING)){
                FinishImageUpload.setArchiveReason(session, false);
                newarchiveLocation = "/data" +PrearcDatabase.archive(session, overrideExceptions, allowSessionMerge,overwrite_files, user, listeners);
            }else{
                this.getResponse().setStatus(Status.CLIENT_ERROR_FORBIDDEN,"Operation already in progress on this prearchive entry.");
            }
            this.fixedSessions = this.fixedSessions +1;
            logger.info("Successfully fixed: "+currentPreArcEntry+"New archive path: "+ newarchiveLocation);
        }catch (Exception e){
            logger.error("Failed fixing: "+currentPreArcEntry+ e.toString());
        }
    }

    /**
     * gets the PrearcSession through the path from metadata
     * @param preArcEntryMetadata containing all metadata of a prearchive entrie
     * @return the prearcsession for the given metadata
     */
    public PrearcSession getPrearcSession(Object[] preArcEntryMetadata){
        String path = preArcEntryMetadata[11].toString();
        currentPreArcEntry = "Current PreArcEntry " + path + ": " + preArcEntryMetadata[0] + "/" + preArcEntryMetadata[1] + "/" + preArcEntryMetadata[6] + "/" + preArcEntryMetadata[7] + "/" + preArcEntryMetadata[8] + "\r\n";
        URIManager.DataURIA data;
        PrearcSession session;

        try {
            data = UriParserUtils.parseURI(path);
        } catch (MalformedURLException e) {
            logger.error("Error accessing session: " + currentPreArcEntry + e.toString());
            return null;
        }
        if (data instanceof URIManager.ArchiveURI) { // check if prearchive session has moved to archive
            logger.error("Error accessing session: " + currentPreArcEntry + "Invalid src URI (" + path + ")");
            return null;
        }
        try {
            session = new PrearcSession((URIManager.PrearchiveURI) data, additionalValues, user); // get preArc session to URI
        } catch (InvalidPermissionException e) {
            logger.error("Error accessing session: " + currentPreArcEntry + e.toString());
            return null;
        } catch (Exception e) {
            logger.error("Error accessing session: " + currentPreArcEntry + e.toString());
            return null;
        }

        if (session.getProject() == null) { // check if session is assigned
            logger.error("Error accessing session: " + currentPreArcEntry + "Session is unassigned");
            return null;
        }
        if (!session.getSessionDir().exists()) { // check if session exists
            logger.error("Error accessing session: " + currentPreArcEntry + session.getUrl() + " not found.");
            return null;
        }
        return session;
    }

    /**
     * Extracts all error codes and concat's sorted each error to a string separated with /
     * @param errors list of notices containing errors and error messages of one prearcsession
     * @return a string representing the combination of errors
     */
    public String getErrorStringfromList(List<PrearcSessionValidator.Notice> errors){
        String errorCombo = "";
        for(int j=0;j<errors.size();j++){
            String code = ""+errors.get(j).getCode();
            if(!errorCombo.contains(code)){
                errorCombo = errorCombo+code+"/";
            }
        }
        return errorCombo;
    }

    /**
     * checks if a given prearc session has errors and conflicts
     * @param session the prearc session
     * @return a list of notices with all errors msg and codes
     */
    private List<PrearcSessionValidator.Notice> getPrearcImportErrors(PrearcSession session) {
        List<PrearcSessionValidator.Notice> errors;
        try {
            PrearcSessionValidator validator = new PrearcSessionValidator(session, user, additionalValues);
            errors= validator.validate();
            Collections.sort(errors, new Comparator<PrearcSessionValidator.Notice>() {
                @Override
                public int compare(PrearcSessionValidator.Notice o1, PrearcSessionValidator.Notice o2) {
                    return o1.getCode() - o2.getCode();
                }
            });
        } catch (Exception ce) {
            logger.error("Error collecting import errors of session: "+ currentPreArcEntry + ce.toString());
            return null;
        }

        return errors;
    }

    /**
     * takes all projects available for the logged in user(PrearcUtils) and checks which are in the prearchive (PrearcDatabase)
     * @return table with all prearchive projects of the logged in user (has to be admin in this class)
     */
    private XFTTable getPrearchiveEntries() {
        ArrayList<String> projects = null;

        try {
            projects = PrearcUtils.getProjects(user, null);
        } catch (Exception e) {
            logger.error("Janitor Error: "+" Unable to get list of projects", e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e.getMessage());
            return null;
        }
        XFTTable table;
        try {
            table = PrearcUtils.convertArrayLtoTable(PrearcDatabase.buildRows(projects.toArray(new String[projects.size()])));
        } catch (SQLException e) {
            logger.error("Janitor Error: "+"Unable to query prearchive table : ", e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e.getMessage());
            return null;
        } catch (SessionException e) {
            logger.error("Janitor Error: "+"Unable to query prearchive table : ", e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e.getMessage());
            return null;
        } catch (Exception e) {
            logger.error("Janitor Error: "+"Unable to query prearchive table : ", e);
            this.getResponse().setStatus(Status.SERVER_ERROR_INTERNAL, e.getMessage());
            return null;
        }
        return table;
    }
}
